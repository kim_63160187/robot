/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kim.robot;

/**
 *
 * @author ASUS
 */
public class TableMap {

    private int width;
    private int height;
    private Robots robots;
    private Bomb bomb;

    public TableMap(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void setRobots(Robots robots) {
        this.robots = robots;
    }

    public void setBomb(Bomb bomb) {
        this.bomb = bomb;
    }

    public void showMap() {
        showTitle();
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if(robots.isOn(x,y)){
                    showRobots();
                } else if(bomb.isOn(x, y)){  
                    showBomb();
                }else {
                    showCell();
                }
            }        
            showNewLine(); 
        }
    }

    private void showTitle() {
        System.out.println("Map");
    }

    private void showNewLine() {
        System.out.println("");
    }

    private void showCell() {
        System.out.println("-");
    }

    private void showBomb() {
        System.out.println(bomb.getSymbol());
    }

    private void showRobots() {
        System.out.println(robots.getSymbol());
    }

    public boolean inMap(int x, int y) {
        // x -> 0-(width-1), y -> 0-(height-1)
        return (x >= 0 && x < width) && (y >= 0 && y < height);
    }

    public boolean isBomb(int x, int y) {
        return bomb.isOn(x, y);
    }

}
